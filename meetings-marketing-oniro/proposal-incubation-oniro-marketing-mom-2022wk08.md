# Oniro WG Incubation stage marketing 2022wk08

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-02-23

## Participants

Dony (Huawei), Chiara (Huawei), Philippe (Huawei), Carlo (Array), Agustin (EF), Aurore (Huawei), Stefan (Huawei), Patrick (Noi Techpark) and Adrian (Huawei)

## Agenda

* MWC 2022 - Agustin 5 min 
* Coming events - Dony 5 min
* Value proposition - Aurore 5 min
* AOB - 10 min


## MoM

### MWC 2022

Meetings

* Confirmed some meetings

Transportation

* Spreadsheet for transportation: details from Oniro filled out.

Presentation

* Work continue in this front
   * MR #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/merge_requests/107>
* Davide Ricci, Bill Fletcher and Marco Sogli online participation confirmed.

Collateral

* Chiara sent a proposal. Review from EF done. Next step?
   * Chiara will review it.
* Check the ticket #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/34>

Promotion of the Oniro presentation on social media

* Check the task for progress #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/35>
   * I propose to close the ticket unless anybody has a different idea/proposal.

Discussion


### Coming events

Presentation

* FOSSNorth
   * Oniro sponsoring
   * Talks submitted. No info yet on approval

* IoTS World Congress
   * May in Barcelona
   * Oniro is sponsoring.
   * Booth.
   * Seco will be on the stage presenting the blueprint.
   * Event collocated with an additional one.
   * Call for participation.
      * Company logo and description required. 
   
Discussion

* Event we are not sponsoring is the Zephyr event #link <https://events.linuxfoundation.org/zephyr-developer-summit/>
   * CFP will close soon. 
* Another interesting event #link <https://www.meetup.com/fr-FR/hdg-france-huawei-developer-group/events/284042519/> 

### Value proposition

Presentation

* Reminder to review the value proposition document. 
   * Feedback required to improve the document.
* A video is planned on how we see Oniro in 5 or 10 years.
   * Call for future use cases. Ping Aurore for an interview.
   * There will be interviews.
   * Collecting ideas to provide scenarios to an agency.

Discussion


### AOB

* Marketing Plan will be explained during the initial Marketing Committee on March 9th
* IP transfer of designs and marketing material from HUawei to EF: discussion at Milan.
* Oniro WG marketing mailing list created.
   * #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
   * Public mailing list with Archive.
   * Please join. Agustin will announce it shortly.

## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
